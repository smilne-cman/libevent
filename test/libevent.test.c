#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include "libevent.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void before_each();
static void test_observable();
static void test_bus();
static void test_global();
static void test_observable_off();
static void test_eventbus_off();
static void test_global_off();

static void handler_recorder_a(void *args);
static void handler_recorder_b(void *args);

/* Global Variables ***********************************************************/
static int recorder_a_calls = 0;
static int recorder_b_calls = 0;

int main(int argc, char *argv[]) {
  test_init("libevent", argc, argv);
  test_before_each(before_each);

  test("Observable", test_observable);
  test("Event Bus", test_bus);
  test("Global Event Bus", test_global);
  test("Observable Off", test_observable_off);
  test("Event Bus Off", test_eventbus_off);
  test("Global Off", test_global_off);

  return test_complete();
}

static void before_each() {
  recorder_a_calls = 0;
  recorder_b_calls = 0;
}

static void test_observable() {
  Observable *observable = observable_new();

  observable_on(observable, "event", handler_recorder_a);
  observable_on(observable, "other", handler_recorder_b);
  observable_fire(observable, "event", NULL);
  observable_fire(observable, "event", NULL);

  test_assert_int(recorder_a_calls, 2, "Should invoke event handlers when event fired");
  test_assert_int(recorder_b_calls, 0, "Should not invoke event handlers for non-fired events");

  observable_destroy(observable);
}

static void test_bus() {
  EventBus *bus = eventbus_new();
  Observable *source_a = observable_new();
  Observable *source_b = observable_new();

  eventbus_register(bus, source_a, "a");
  eventbus_register(bus, source_b, "b");

  eventbus_listen(bus, "a", "event", handler_recorder_a);
  eventbus_listen(bus, "b", "event", handler_recorder_b);

  eventbus_fire(bus, "a", "event", NULL);
  eventbus_fire(bus, "b", "event", NULL);
  eventbus_fire(bus, "a", "event", NULL);

  test_assert_int(recorder_a_calls, 2, "Should invoke first handler");
  test_assert_int(recorder_b_calls, 1, "Should invoke second handler");

  observable_destroy(source_a);
  observable_destroy(source_b);
  eventbus_new(bus);
}

static void test_global() {
  Observable *source_a = observable_new();
  Observable *source_b = observable_new();

  global_register(source_a, "a");
  global_register(source_b, "b");

  global_listen("a", "event", handler_recorder_a);
  global_listen("b", "event", handler_recorder_b);

  global_fire("a", "event", NULL);
  global_fire("b", "event", NULL);
  global_fire("a", "event", NULL);

  test_assert_int(recorder_a_calls, 2, "Should invoke first handler");
  test_assert_int(recorder_b_calls, 1, "Should invoke second handler");

  observable_destroy(source_a);
  observable_destroy(source_b);
}

static void test_observable_off() {
  Observable *observable = observable_new();

  observable_on(observable, "event", handler_recorder_a);
  observable_fire(observable, "event", NULL);

  test_assert_int(recorder_a_calls, 1, "Should invoke handler before off()");

  observable_off(observable, "event", handler_recorder_a);
  observable_fire(observable, "event", NULL);

  test_assert_int(recorder_a_calls, 1, "Should not invoke handler after off()");

  observable_destroy(observable);
}

static void test_eventbus_off() {
  Observable *observable = observable_new();
  EventBus *bus = eventbus_new();

  eventbus_register(bus, observable, "observable");
  eventbus_listen(bus, "observable", "event", handler_recorder_a);
  eventbus_fire(bus, "observable", "event", NULL);

  test_assert_int(recorder_a_calls, 1, "Should invoke handler before off()");

  eventbus_off(bus, "observable", "event", handler_recorder_a);
  eventbus_fire(bus, "observable", "event", NULL);

  test_assert_int(recorder_a_calls, 1, "Should not invoke handler after off()");

  eventbus_destroy(bus);
  observable_destroy(observable);
}

static void test_global_off() {
  Observable *observable = observable_new();

  global_register(observable, "observable");
  global_listen("observable", "event", handler_recorder_a);
  global_fire("observable", "event", NULL);

  test_assert_int(recorder_a_calls, 1, "Should invoke handler before off()");

  global_off("observable", "event", handler_recorder_a);
  global_fire("observable", "event", NULL);

  test_assert_int(recorder_a_calls, 1, "Should not invoke handler after off()");

  observable_destroy(observable);
}

static void handler_recorder_a(void *args) {
  recorder_a_calls++;
}

static void handler_recorder_b(void *args) {
  recorder_b_calls++;
}
