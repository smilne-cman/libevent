#ifndef __libevent__
#define __libevent__

/* Includes *******************************************************************/
#include <libcollection/map.h>

/* Types **********************************************************************/
typedef void (*EventHandler)(void *arguments);

typedef struct EventBus {
  Map *observables;
} EventBus;

typedef struct Observable {
  Map *listeners;
} Observable;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/

EventBus *eventbus_new();
Observable *observable_new();

void eventbus_destroy(EventBus *bus);
void observable_destroy(Observable *observable);

void observable_on(Observable *observable, const char *event, EventHandler handler);
void observable_fire(Observable *observable, const char *event, void *arguments);
void observable_off(Observable *observable, const char *event, EventHandler handler);

void eventbus_register(EventBus *bus, Observable *observable, const char *name);
void eventbus_listen(EventBus *bus, const char *observable, const char *event, EventHandler handler);
void eventbus_fire(EventBus *bus, const char *observable, const char *event, void *arguments);
void eventbus_off(EventBus *bus, const char *observable, const char *event, EventHandler handler);

void global_register(Observable *observable, const char *name);
void global_listen(const char *observable, const char *event, EventHandler handler);
void global_fire(const char *observable, const char *event, void *arguments);
void global_off(const char *observable, const char *event, EventHandler handler);

#endif
