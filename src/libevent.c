#include <stdio.h>
#include <stdlib.h>
#include <libcollection/map.h>
#include <libcollection/list.h>
#include <libcollection/compare.h>
#include <libcollection/iterator.h>
#include "libevent.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static EventBus *get_global();

/* Global Variables ***********************************************************/
static EventBus *global_eventbus = NULL;

/* Functions ******************************************************************/

EventBus *eventbus_new() {
  EventBus *bus = (EventBus *)malloc(sizeof(EventBus));

  bus->observables = map_new(map_by_string(void_compare));

  return bus;
}

Observable *observable_new() {
  Observable *observable = (Observable *)malloc(sizeof(Observable));

  observable->listeners = map_new(map_by_string(list_compare));

  return observable;
}

void eventbus_destroy(EventBus *bus) {
  map_destroy(bus->observables);
  free(bus);
}

void observable_destroy(Observable *observable) {
  map_destroy(observable->listeners);
  free(observable);
}

void observable_on(Observable *observable, const char *event, EventHandler handler) {
  List *handlers = map_get(observable->listeners, event);

  if (handlers == NULL) {
    handlers = list_new(ListAny);
    map_put(observable->listeners, event, handlers);
  }

  list_add(handlers, handler);
}

void observable_fire(Observable *observable, const char *event, void *arguments) {
  List *handlers = map_get(observable->listeners, event);

  if (handlers == NULL) {
    return;
  }

  Iterator *iterator = list_iterator(handlers);
  while(has_next(iterator)) {
    EventHandler handler = (EventHandler)next(iterator);

    handler(arguments);
  }
}

void observable_off(Observable *observable, const char *event, EventHandler handler) {
  List *handlers = map_get(observable->listeners, event);
  if (handlers == NULL) {
    return;
  }

  int index = list_index_of(handlers, handler);
  if (index >= 0) {
    list_remove(handlers, index);
  }
}

void eventbus_register(EventBus *bus, Observable *observable, const char *name) {
  map_put(bus->observables, name, observable);
}

void eventbus_listen(EventBus *bus, const char *observable, const char *event, EventHandler handler) {
  Observable *thing = map_get(bus->observables, observable);

  if (thing != NULL) {
    observable_on(thing, event, handler);
  }
}

void eventbus_fire(EventBus *bus, const char *observable, const char *event, void *arguments) {
  Observable *thing = map_get(bus->observables, observable);

  if (thing != NULL) {
    observable_fire(thing, event, arguments);
  }
}

void eventbus_off(EventBus *bus, const char *observable, const char *event, EventHandler handler) {
  Observable *thing = map_get(bus->observables, observable);

  if (thing != NULL) {
    observable_off(thing, event, handler);
  }
}

void global_register(Observable *observable, const char *name) {
  eventbus_register(get_global(), observable, name);
}

void global_listen(const char *observable, const char *event, EventHandler handler) {
  eventbus_listen(get_global(), observable, event, handler);
}

void global_fire(const char *observable, const char *event, void *arguments) {
  eventbus_fire(get_global(), observable, event, arguments);
}

void global_off(const char *observable, const char *event, EventHandler handler) {
  eventbus_off(get_global(), observable, event, handler);
}

static EventBus *get_global() {
  if (global_eventbus == NULL) {
    global_eventbus = eventbus_new();
  }

  return global_eventbus;
}
